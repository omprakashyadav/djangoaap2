from django.urls import path

from . import views

urlpatterns = [
    path('', views.index_view, name='index'),
    path('create', views.create_view, name='create'),
    path('delete', views.delete_view, name='delete'),
    path('detail', views.detail_view, name='detail'),
    path('edit', views.edit_view, name='edit'),
    
]
