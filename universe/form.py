from django import forms

class UniverseForm(forms.Form):
    name =forms.CharField()
    description = forms.CharField()
    index = forms.IntegerField()
    age = forms.IntegerField()
    satellites = forms.CharField()
    radius = forms.IntegerField()
    volume = forms.IntegerField()
    mass= forms.IntegerField()
    density = forms.IntegerField()
    distance_from_sun = forms.IntegerField()
    distance_from_earth = forms.IntegerField()
    day_length = forms.IntegerField()
