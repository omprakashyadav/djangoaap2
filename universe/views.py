from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.shortcuts import render 
from django.shortcuts import (get_object_or_404,
                              render,
                              HttpResponseRedirect)

from .forms import UniverseForm
from .models import planets

def index_view(request):
    return render('', 'index.html')

def create_view(request):
        if request.method == 'POST':
            # create a form instance and populate it with data from the request:
            form = UniverseForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('')

    # if a GET (or any other method) we'll create a blank form
        else:
            form = UniverseForm()

        return render(request, 'create.html', {'form': form})



def delete_view(request,id):
    context = {}

    # fetch the object related to passed id
    obj = get_object_or_404(planets, id = id)

    if request.method == "POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        # home page
        return HttpResponseRedirect("/") 

    return render(request, "delete_view.html", context)
    


def detail_view(request, id):
    context = {}

    # add the dictionary during initialization
    context["data"] = planets.objects.get(id=id)

    return render(request, "detail_view.html", context)

# update view for details


def edit_view(request, id):
    # dictionary for initial data with
    # field names as keys
    context = {}

    # fetch the object related to passed id
    obj = get_object_or_404(planets, id=id)

    # pass the object as instance in form
    form = UniverseForm(request.POST or None, instance=obj)

    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/"+id)

    # add form dictionary to context
    context["form"] = form

    return render(request, "edit_view.html", context)

