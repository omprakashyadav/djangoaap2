from django.db import models

# Create your models here.
class planets(models.Model):
    name =models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    index = models.IntegerField()
    age = models.IntegerField()
    satellites = models.CharField(max_length=30)
    radius = models.IntegerField()
    volume = models.IntegerField()
    mass= models.IntegerField()
    density = models.IntegerField()
    distance_from_sun = models.IntegerField()
    distance_from_earth = models.IntegerField()
    day_length = models.IntegerField()
